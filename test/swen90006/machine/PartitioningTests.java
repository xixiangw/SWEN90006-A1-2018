package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;
import java.util.concurrent.TimeoutException;

import org.junit.*;
import org.omg.CORBA.DynAnyPackage.Invalid;

import static org.junit.Assert.*;

public class PartitioningTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  // ======= 10 operands =======
  // Arithmetic: ADD/SUB/MUL/DIV
  @Test(expected = InvalidInstructionException.class)
  public void arith_add_para_test()
  throws Throwable
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("MOV R1 2");
    lines.add("MOV R2 3");
    lines.add("ADD R3 R1");
    lines.add("ADD R3");
    lines.add("ADD");
    lines.add("ADD R3 R1 R4 R5");
    lines.add("RET R3");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test public void arith_add_test()
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("MOV R1 2");
    lines.add("MOV R2 3");
    lines.add("ADD R3 R1 R2");
    lines.add("RET R3");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 5);
  }

  @Test(expected = InvalidInstructionException.class)
  public void arith_sub_para_test()
          throws Throwable
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("MOV R1 2");
    lines.add("MOV R2 3");
    lines.add("SUB R3 R1");
    lines.add("SUB R3");
    lines.add("SUB");
    lines.add("SUB R3 R1 R4 R5");
    lines.add("RET R3");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test public void arith_sub_test()
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("MOV R1 2");
    lines.add("MOV R2 3");
    lines.add("SUB R3 R1 R2");
    lines.add("RET R3");
    Machine m = new Machine();
    assertEquals(m.execute(lines), -1);
  }

  @Test(expected = InvalidInstructionException.class)
  public void arith_mul_para_test()
          throws Throwable
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("MOV R1 2");
    lines.add("MOV R2 3");
    lines.add("MUL R3 R1");
    lines.add("MUL R3");
    lines.add("MUL");
    lines.add("MUL R3 R1 R4 R5");
    lines.add("RET R3");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test public void arith_mul_test()
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("MOV R1 -2");
    lines.add("MOV R2 -3");
    lines.add("MUL R3 R1 R2");
    lines.add("RET R3");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 6);
  }

  @Test(expected = InvalidInstructionException.class)
  public void arith_div_para_test()
          throws Throwable
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("MOV R1 2");
    lines.add("MOV R2 3");
    lines.add("DIV R3 R1");
    lines.add("DIV R3");
    lines.add("DIV");
    lines.add("DIV R3 R1 R4 R5");
    lines.add("RET R3");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test public void arith_div_test()
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("MOV R1 -9");
    lines.add("MOV R2 3");
    lines.add("DIV R3 R1 R2");
    lines.add("RET R3");
    Machine m = new Machine();
    assertEquals(m.execute(lines), -3);
  }

  @Test(expected = InvalidInstructionException.class)
  public void mov_para_test()
          throws Throwable
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("MOV R1 2");
    lines.add("MOV ");
    lines.add("MOV R1");
    lines.add("MOV 2");
    lines.add("RET R3");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test public void mov_test()
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("MOV R0 2");
    lines.add("RET R0");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 2);
  }

  @Test(expected = InvalidInstructionException.class)
  public void ret_para_test()
          throws Throwable
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("RET");
    lines.add("RET R3 R1");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test public void ret_test()
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("RET R0");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }

  @Test(expected = InvalidInstructionException.class)
  public void ldr_para_test()
          throws Throwable
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("LDR");
    lines.add("LDR R1 2");
    lines.add("LDR R1 R2 0 R3");
    lines.add("RET R0");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test(expected = InvalidInstructionException.class)
  public void str_para_test()
          throws Throwable
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("STR");
    lines.add("STR 2");
    lines.add("STR R1 3");
    lines.add("STR R1 R2 0 R3");
    lines.add("RET R0");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test(expected = InvalidInstructionException.class)
  public void jmp_para_test()
          throws Throwable
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("JMP");
    lines.add("JMP R1 3");
    lines.add("JMP R1");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test(expected = InvalidInstructionException.class)
  public void jz_para_test()
          throws Throwable
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("JZ");
    lines.add("JZ 3");
    lines.add("JZ R5");
    lines.add("JZ R0 5 10");
    lines.add("JZ 5 10");
    Machine m = new Machine();
    m.execute(lines);
  }

  public void jmp_jz_pair_test()
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("MOV R3 10");
    lines.add("MOV R2 1");
    lines.add("MOV R1 0");
    lines.add("MOV R0 100");
    lines.add("SUB R4 R3 R1");
    lines.add("JZ  R4 5");
    lines.add("STR R0 0  R1");
    lines.add("ADD R1 R1 R2");
    lines.add("ADD R0 R0 R2");
    lines.add("JMP -3");

    lines.add("MOV R1 0");
    lines.add("MOV R0 100");
    lines.add("MOV R5 0");

    lines.add("LDR R4 R0 2");
    lines.add("ADD R5 R5 R4");

    lines.add("RET R5");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 0);
  }

  @Test
  public void infiniteLoop() throws Exception {
    Thread t = new Thread(new Runnable() {
      public void run() {
        jmp_jz_pair_test();
      }
    });
    t.start();
    t.join(2000);
    assertTrue(t.isAlive());
    t.interrupt(); // dont know if this works
  }

  @Test public void ldr_str_jmp_jz_test()
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("MOV R3 10");
    lines.add("MOV R2 1");
    lines.add("MOV R1 0");
    lines.add("MOV R0 100");
    lines.add("SUB R4 R3 R1");
    lines.add("JZ  R4 5");
    lines.add("STR R0 0  R1");
    lines.add("ADD R1 R1 R2");
    lines.add("ADD R0 R0 R2");
    lines.add("JMP -5");
    lines.add("MOV R1 0");
    lines.add("MOV R0 100");
    lines.add("MOV R5 0");
    lines.add("SUB R4 R3 R1");
    lines.add("JZ  R4 6");
    lines.add("LDR R4 R0 0");
    lines.add("ADD R5 R5 R4");
    lines.add("ADD R0 R0 R2");
    lines.add("ADD R1 R1 R2");
    lines.add("JMP -6");
    lines.add("RET R5");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 45);
  }


  @Test(expected = NoReturnValueException.class)
  public void no_retrun_test()
          throws Throwable
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("MOV R1 10");
    lines.add("MOV R2 10");
    Machine m = new Machine();
    m.execute(lines);
  }


  //Any method annotation with "@Test" is executed as a test.
  @Test public void aTest()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
    final int expected = 2;
    final int actual = 1 + 1;
    assertEquals(expected, actual);
  }

  @Test public void anotherTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("a");
    list.add("b");

    //the assertTrue method is used to check whether something holds.
    assertTrue(list.contains("a"));
  }

  //Test test opens a file and executes the machine
  @Test public void aFileOpenTest()
  {
    final List<String> lines = readInstructions("examples/array.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 45);
  }
  
  //To test an exception, specify the expected exception after the @Test
  @Test(expected = java.io.IOException.class) 
    public void anExceptionTest()
    throws Throwable
  {
    throw new java.io.IOException();
  }

  //This test should fail.
  //To provide additional feedback when a test fails, an error message
  //can be included
//  @Test public void aFailedTest()
//  {
//    //include a message for better feedback
//    final int expected = 2;
//    final int actual = 1 + 2;
//    assertEquals("Some failure message", expected, actual);
//  }

  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }


}
