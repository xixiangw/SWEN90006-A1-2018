package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  //Any method annotation with "@Test" is executed as a test.
  @Test public void aTest()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
    final int expected = 2;
    final int actual = 1 + 1;
    assertEquals(expected, actual);
  }

  @Test
  public void value_min_on_point_boundary_test() {
    ArrayList var1 = new ArrayList();
    var1.add("MOV R1 -65535");
    var1.add("RET R1");
    Machine var2 = new Machine();
    Assert.assertEquals((long)var2.execute(var1), -65535);
  }

  @Test(expected = InvalidInstructionException.class)
  public void value_min_off_point_boundary_test()
          throws Throwable
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("MOV R1 -65536");
    lines.add("RET R1");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test
  public void value_max_on_point_boundary_test() {
    ArrayList var1 = new ArrayList();
    var1.add("MOV R1 65535");
    var1.add("RET R1");
    Machine var2 = new Machine();
    Assert.assertEquals((long)var2.execute(var1), 65535);
  }


  @Test(expected = InvalidInstructionException.class)
  public void value_max_off_point_boundary_test()
          throws Throwable
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("MOV R1 65536");
    lines.add("RET R1");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test(expected = InvalidInstructionException.class)
  public void register_max_off_point_boundary_test()
          throws Throwable
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("MOV R-1 1");
    lines.add("RET R-1");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test(expected = InvalidInstructionException.class)
  public void register_min_off_point_boundary_test()
          throws Throwable
  {
    final List<String> lines = new ArrayList<String>();;
    lines.add("MOV R32 1");
    lines.add("RET R32");
    Machine m = new Machine();
    m.execute(lines);
  }

  @Test
  public void register_min_on_point_boundary_test() {
    ArrayList var1 = new ArrayList();
    var1.add("MOV R0 1");
    var1.add("RET R0");
    Machine var2 = new Machine();
    Assert.assertEquals((long)var2.execute(var1), 1);
  }

  @Test
  public void register_max_on_point_boundary_test() {
    ArrayList var1 = new ArrayList();
    var1.add("MOV R31 1");
    var1.add("RET R31");
    Machine var2 = new Machine();
    Assert.assertEquals((long)var2.execute(var1), 1);
  }


  // Should overflow, but the system does not give a correct answer?
  @Test
  public void overflow_max_on_point_boundary_test() {
    ArrayList var1 = new ArrayList();
    var1.add("MOV R3 4000");
    var1.add("MOV R2 1");
    var1.add("MOV R1 0");
    var1.add("MOV R0 100");
    var1.add("SUB R4 R3 R1");
    var1.add("JZ  R4 5");
    var1.add("STR R0 0  R1");
    var1.add("ADD R1 R1 R2");
    var1.add("ADD R0 R0 R2");
    var1.add("JMP -5");
    var1.add("MOV R1 0");
    var1.add("MOV R0 100");
    var1.add("MOV R5 0");
    var1.add("SUB R4 R3 R1");
    var1.add("JZ  R4 6");
    var1.add("LDR R4 R0 0");
    var1.add("ADD R5 R5 R4");
    var1.add("ADD R0 R0 R2");
    var1.add("ADD R1 R1 R2");
    var1.add("JMP -6");
    var1.add("RET R5");
    Machine var2 = new Machine();
    Assert.assertEquals((long)var2.execute(var1), 7998000);
  }



  @Test public void anotherTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("a");
    list.add("b");

    //the assertTrue method is used to check whether something holds.
    assertTrue(list.contains("a"));
  }

  //Test test opens a file and executes the machine
  @Test public void aFileOpenTest()
  {
    final List<String> lines = readInstructions("examples/array.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 45);
  }
  
  //To test an exception, specify the expected exception after the @Test
  @Test(expected = java.io.IOException.class) 
    public void anExceptionTest()
    throws Throwable
  {
    throw new java.io.IOException();
  }

  //This test should fail.
  //To provide additional feedback when a test fails, an error message
  //can be included
//  @Test public void aFailedTest()
//  {
//    //include a message for better feedback
//    final int expected = 2;
//    final int actual = 1 + 2;
//    assertEquals("Some failure message", expected, actual);
//  }

  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
}
